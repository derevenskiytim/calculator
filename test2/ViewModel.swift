//
//  ViewModel.swift
//  test2
//
//  Created by Artem Chernyshov on 01.11.2021.
//

import Foundation

// swiftlint:disable indentation_width
public protocol ViewModel {
    /// Входящие от `View` евенты
    associatedtype Input
    /// Исходящие от `ViewModel` увенты
    associatedtype Output

    /**
     Принимает входящие от `View` евенты и отдает результирующие евенты из `ViewModel` во `View`.

     Вызовите этот метод из `setupOutput` во `View`.

     Передайте результирующие евенты в `outputHandler`.

     # Внимание! Метод не должен содержать логику. Вся логика должна выносится в отдельные методы!

     # Пример
     ```
     func transform(_ input: SomeViewModel.Input, outputHandler: (SomeViewModel.Output) -> Void) {
         let output = Output()
         outputHandler(output)
     }
     ```

     - parameters:
        - input: Входящие от `View` евенты
        - outputHandler: Хендлер с результирующими евентами
        - output: Результирующие евенты
     */
    func transform(_ input: Input, outputHandler: @escaping (_ output: Output) -> Void)
}

// swiftlint:enabble indentation_width
