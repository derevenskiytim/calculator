//
//  ViewController.swift
//  test2
//
//  Created by Artyom Chernyshov on 24.10.2021.
//

import UIKit
import RxCocoa
import RxRelay
import RxSwift
import RxGesture
import RxAppState
import Moya

class ViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var exchangeRatesTitle: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var tableView: SelfSizedTableView!
    @IBOutlet weak var swipeButton: UIButton!
    
    @IBOutlet weak var priceOfProductTextField: UITextField!
    @IBOutlet weak var deliveryPriceTextField: UITextField!
    @IBOutlet weak var weightOfProductTextField: UITextField!
    
    @IBOutlet weak var priceOfProductLabel: UILabel!
    @IBOutlet weak var finalAmountOfDutyLabel: UILabel!
    @IBOutlet weak var finalPriceLabel: UILabel!
    
    // MARK: - Properties
    
    let viewModel = СustomsTaxCalculatorViewModel()
    
    private let viewDidLoadPublishRelay = PublishRelay<Void>()
    
    private var itemsArray: [String] = []
    
    private var disposeBag = DisposeBag()

    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDidLoadPublishRelay.accept(())
        
        setupOutput()
        
        tableView.isHidden = true
        tableView.maxHeight = 240
        tableView.register(TableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.delegate = self
        tableView.dataSource = self
        
        setupSwipeButtonObserving()
        
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(sender:)))
        gesture.cancelsTouchesInView = false
        view.addGestureRecognizer(gesture)
    }
    
    // MARK: - SetupOutput
    
    public func setupOutput() {
        let input = СustomsTaxCalculatorViewModel.Input(
            viewDidLoad: .just(()),
            priceOfProductTextFieldEditing: priceOfProductTextField.rx.controlEvent(.editingChanged).map({ _ in
                self.priceOfProductTextField.text.orEmpty
            }),
            deliveryPriceTextFieldEditing: deliveryPriceTextField.rx.controlEvent(.editingChanged).map({ _ in
                self.deliveryPriceTextField.text.orEmpty
            }),
            weightOfProductTextFieldEditing: weightOfProductTextField.rx.controlEvent(.editingChanged).map({ _ in
                self.weightOfProductTextField.text.orEmpty
            }),
            disposeBag: disposeBag
        )

        viewModel.transform(input, outputHandler: setupInput(input:))
    }

    public func setupInput(input: СustomsTaxCalculatorViewModel.Output) {
        setupTableViewItemsObserving(with: input.tableItems)
        setupExchangeRatesTitleObserving(with: input.exchangeRatesTitle)
        setupPriceOfProductTitleObserving(with: input.priceOfProductTitle)
        setupFinalAmountOfDutyTitleObserving(with: input.finalAmountOfDutyTitle)
        setupFinalPriceLabelObserving(with: input.finalPriceTitle)
    }
    
    // MARK: - Observing function
    
    private func setupTableViewItemsObserving(with signal: Observable<[String]>) {
        signal.asObservable()
            .subscribe(onNext: {
                self.itemsArray = $0
                self.tableView.reloadData()
            })
    }
    
    private func setupExchangeRatesTitleObserving(with signal: Driver<String>) {
        signal
            .drive(exchangeRatesTitle.rx.text)
    }
    
    private func setupPriceOfProductTitleObserving(with signal: Driver<String>) {
        signal
            .map({ text in
                let doubleText = Double(text) ?? 0.00
                let formattedText = String(format: "%.2f", doubleText)
                return formattedText + " руб."
            })
            .drive(priceOfProductLabel.rx.text)
    }
    
    private func setupFinalAmountOfDutyTitleObserving(with signal: Driver<String>) {
        signal
            .drive(finalAmountOfDutyLabel.rx.text)
    }
    
    private func setupFinalPriceLabelObserving(with signal: Driver<String>) {
        signal
            .drive(finalPriceLabel.rx.text)
    }
    
    private func setupSwipeButtonObserving() {
        swipeButton.rx.tap.subscribe(onNext: {
            let isSelected = !self.swipeButton.isSelected
            self.swipeButton.isSelected = isSelected
            
            switch self.swipeButton.isSelected {
            case true:
                UIView.animate(
                    withDuration: 1,
                    delay: 0,
                    usingSpringWithDamping: 0.9,
                    initialSpringVelocity: 1,
                    options: [],
                    animations: {
                        self.tableView.isHidden = false
                        self.swipeButton.setImage(UIImage(named: "down-arrow-1"), for: .normal)
                        self.tableView.layoutIfNeeded()
                    },
                    completion: nil)
                
            case false:
                UIView.animate(
                    withDuration: 0.5,
                    delay: 0,
                    usingSpringWithDamping: 0.9,
                    initialSpringVelocity: 1,
                    options: [],
                    animations: {
                        self.tableView.isHidden = true
                        self.swipeButton.setImage(UIImage(named: "down-arrow"), for: .normal)
                        self.tableView.layoutIfNeeded()
                    },
                    completion: nil)
            }
        })
    }
    
    // MARK: -  public Function
    
    // MARK: -  private Function
    
    @objc
    private func dismissKeyboard(sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
}

// MARK: - Extension ViewController

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        cell.configure(title: itemsArray[indexPath.row])
        
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        textField.text = itemsArray[indexPath.row]
        
        viewModel.didSelectCell(with: itemsArray[indexPath.row])
    }
}


public extension Optional where Wrapped == String {
    /// If self is not optional returns self otherwise an empty string
    var orEmpty: String {
        return self ?? ""
    }
}
