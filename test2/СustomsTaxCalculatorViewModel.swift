//
//  ViewModel.swift
//  test2
//
//  Created by Artyom Chernyshov on 27.10.2021.
//

import Foundation
import RxSwift
import RxCocoa
import RxRelay
import RxGesture
import Moya

struct FinalPrice {
    var price: Double = 0.0
    var amount: Double = 0.0
    var delivery: Double = 0.0
}

class СustomsTaxCalculatorViewModel: NSObject, ViewModel {
    // MARK: - Constants
    
    public struct Input {
        let viewDidLoad: Observable<Void>
        let priceOfProductTextFieldEditing: Observable<String>
        let deliveryPriceTextFieldEditing: Observable<String>
        let weightOfProductTextFieldEditing: Observable<String>
        let disposeBag: DisposeBag
    }
    
    public struct Output {
        let tableItems: Observable<[String]>
        let exchangeRatesTitle: Driver<String>
        let priceOfProductTitle: Driver<String>
        let finalAmountOfDutyTitle: Driver<String>
        let finalPriceTitle: Driver<String>
    }
    
    // MARK: - Private Properties
    
    private let url = URL(string: "https://www.cbr-xml-daily.ru/daily_json.js")!
    
    private let provider = MoyaProvider<MoyaDownloadImageServise>()
    
    private var arrayItems: [String] = []
    
    private var exchangeRatesDictionary: [String : Double] = [:]
    
    private var euroExchangeRate = 0.0
    
    // MARK: - Private Observables
    
    private let obserbableItems = PublishSubject<[String]>()
    
    private let exchangeRatesTitlePublishSubject = PublishSubject<String>()
    
    private let priceOfProductTitlePublishSubject = BehaviorSubject<String>(value: "0.00")
    
    private let finalAmountOfDutyTitlePublishSubject = BehaviorSubject<String>(value: "0.00")
    
    private let deliveryPriceTextPublishSubject = BehaviorSubject<String>(value: "0.00")
    
    private let weightOfProductTextPublishSubject = BehaviorSubject<String>(value: "0.00")
    
    private let finalPriceTitlePublishSubject = BehaviorSubject<String>(value: "0.00")
    
    // MARK: - Transform
    
    public func transform(_ input: Input, outputHandler: @escaping (Output) -> Void) {
        setupViewDidLoad(with: input.viewDidLoad)
        setupPriceOfProductTextFieldEditing(with: input.priceOfProductTextFieldEditing)
        setupFinalAmountOfDutyLabel()
        setupDeliveryPriceTextFieldEditing(with: input.deliveryPriceTextFieldEditing)
        setupWeightOfProductTextFieldEditing(with: input.weightOfProductTextFieldEditing)
        setupFinalPriceLabel()

        let output = Output(
            tableItems: obserbableItems,
            exchangeRatesTitle: exchangeRatesTitlePublishSubject.asDriver(onErrorJustReturn: ""),
            priceOfProductTitle: priceOfProductTitlePublishSubject.asDriver(onErrorJustReturn: ""),
            finalAmountOfDutyTitle: finalAmountOfDutyTitlePublishSubject.asDriver(onErrorJustReturn: ""),
            finalPriceTitle: finalPriceTitlePublishSubject.asDriver(onErrorJustReturn: "")
        )
        
        outputHandler(output)
    }
    
    // MARK: - Public Functions
    
    public func didSelectCell(with value: String) {
        guard let cource = exchangeRatesDictionary[value] else { return }

        exchangeRatesTitlePublishSubject.onNext("\(String(format: "%.2f", cource))")
    }
    
    // MARK: - Private Functions
    
    private func setupViewDidLoad(with observing: Observable<Void>) {
        observing
            .subscribe(onNext: {
                print("началась загрузка экрана")
                self.networkRequest()
            })
    }
    
    private func setupPriceOfProductTextFieldEditing(with observing: Observable<String>) {
        let bothSequence = Observable.combineLatest(observing, exchangeRatesTitlePublishSubject) { (count, course) -> [String] in
            let array = [count, course]
            return array
        }

        bothSequence.subscribe(onNext: {
            let count = Double($0[0]) ?? 0
            let course = Double($0[1]) ?? 0
            let result = count * course
            
            self.priceOfProductTitlePublishSubject.onNext("\(result)")
        })
    }
    
    private func setupDeliveryPriceTextFieldEditing(with observing: Observable<String>) {
        observing
            .subscribe(onNext: { text in
                self.deliveryPriceTextPublishSubject.onNext(text)
            })
    }
    
    private func setupWeightOfProductTextFieldEditing(with observing: Observable<String>) {
        observing
            .subscribe(onNext: { text in
                self.weightOfProductTextPublishSubject.onNext(text)
            })
    }
    
    private func setupFinalAmountOfDutyLabel() {
        priceOfProductTitlePublishSubject
            .subscribe(onNext: { finalPrice in
                let twoHundredEurosToRubles = self.euroExchangeRate * 200
                let finalPrice = Double(finalPrice) ?? 0
                
                if finalPrice > twoHundredEurosToRubles {
                    let finalAmountOfDuty = String((finalPrice - twoHundredEurosToRubles) * 0.15)
                    self.finalAmountOfDutyTitlePublishSubject.onNext(finalAmountOfDuty)
                } else {
                    self.finalAmountOfDutyTitlePublishSubject.onNext("0.0")
                }
            })
    }
    
    private func setupFinalPriceLabel() {
        let bothSequence = Observable.combineLatest(priceOfProductTitlePublishSubject, finalAmountOfDutyTitlePublishSubject, deliveryPriceTextPublishSubject) { price, amount, delivery -> FinalPrice  in
            let price = Double(price) ?? 0.0
            let amount = Double(amount) ?? 0.0
            let delivery = Double(delivery) ?? 0.0
            
            let finalPrice = FinalPrice(price: price, amount: amount, delivery: delivery)
            return finalPrice
        }
        
        bothSequence
            .subscribe(onNext: { finalPrice in
                let finalPrice = String(finalPrice.price + finalPrice.amount + finalPrice.delivery)
                
                self.finalPriceTitlePublishSubject.onNext(finalPrice)
            })
    }
    
    private func networkRequest() {
        let requestObservable = provider.rx.request(.getImage(url: url)).asObservable().share()
        
        requestObservable.subscribe {
            switch $0.event {
            case let .next(response):
                let model = try? response.map(Model.self)
                guard let model = model else { return }
                
                self.converterItems(with: model)
                
            case let .error(error):
                print(error)
            case .completed:
                print("completed")
            }
        }
    }
    
    private func converterItems(with model: Model) {
        guard let valute = model.valute else { return }
        
        for (_, value) in valute {
            guard let name = value.name,
                  let values = value.value,
                  let nominal = value.nominal,
                  let charCode = value.charCode
            else { return }
            
            arrayItems.append(name)
            
            let finalValue = values / Double(nominal)
            exchangeRatesDictionary.updateValue(finalValue, forKey: name)
            
            if charCode == "EUR" {
                self.euroExchangeRate = finalValue
            }
        }
        
        obserbableItems.onNext(arrayItems)
    }
}

enum MoyaDownloadImageServise {
    case getImage(url: URL)
}

extension MoyaDownloadImageServise: TargetType {
    var baseURL: URL {
        switch self {
        case .getImage(let url):
            return url
        }
    }
    
    var path: String {
        ""
    }
    
    var method: Moya.Method {
        switch self {
        case .getImage:
            return .get
        }
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .getImage:
            return nil
        }
    }
    
    var sampleData: Data {
        .init()
    }
    
    var task:Task {
        switch self {
        case .getImage:
            //return .downloadDestination(DefaultDownloadDestination)
            return .requestPlain
        }
    }
    
    var headers: [String: String]? {
        return nil
    }
}
